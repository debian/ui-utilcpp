// Local configuration
#include "config.h"

// STDC++
#include <iostream>

// POSIX C

// SYSTEM C

// C++ Libraries
#include <ui-utilcpp/GetOpt.hpp>

int main(int argc, char *argv[])
{
	int retValue(0);

	// Define Command Line Arguments
	UI::Util::GetOpt getOpt(argc, argv);
	getOpt
		.set("version",   'v', UI::Util::GetOpt::NoArg_,  "Show version information.")
		.set("help",      'h', UI::Util::GetOpt::NoArg_,  "Show usage information.")
		.set("noarg",     'n', UI::Util::GetOpt::NoArg_,  "Option with no argument.")
		.set("arg",       'a', UI::Util::GetOpt::Arg_,    "Option with mandatory argument.")
		.set("optarg",    'o', UI::Util::GetOpt::OptArg_, "Option with optional argument.");

	// Process standard options
	if (!getOpt.isValid())
	{
		retValue = getOpt.wrongUsage("Parse error");
	}
	else if (getOpt.get("version")->isGiven())
	{
		std::cout << "Release: " << PACKAGE << "-" << VERSION << "." << std::endl << std::endl;
	}
	else if (getOpt.get("help")->isGiven())
	{
		std::cout << std::endl << "Blah tool to do blah." << std::endl;
		getOpt.printUsage();
	}
	else
	{
		// Go down all other options sequentially
		if (getOpt.get("noarg")->isGiven())
		{
			std::cout << "Option \"noarg\" given." << std::endl;
		}

		if (getOpt.get("arg")->isGiven())
		{
			std::cout << "Option \"arg\" given." << std::endl;
			std::cout << "Argument is: " << getOpt.get("arg")->getArg() << std::endl;
		}

		if (getOpt.get("optarg")->isGiven())
		{
			std::cout << "Option \"arg\" given." << std::endl;
			std::cout << "Argument is: " << getOpt.get("optarg")->getArg() << std::endl;
		}

		// Here, some main program code starts
		std::cout << "Program Main Run" << std::endl;

		std::cout << "Try --help if you want to test GetOpt" << std::endl;
	}
	return retValue;
}
