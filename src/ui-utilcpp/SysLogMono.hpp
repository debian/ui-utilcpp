/**
 * @file SysLogMono.hpp
 * @author Schlund + Partner AG
 * @brief Syslog Mono Log: Singleton logger using syslog() for mono threaded applications
 * @see SMLogMono.hpp
 *
 * Synopsis:
 * @code
 * #include <ui-utilcpp/SMLogMono.hpp>
 * @endcode
 *
 */
#ifndef	UI_UTIL_SYSLOGMONO_HPP
#define	UI_UTIL_SYSLOGMONO_HPP

// STDC++
#include <string>
#include <iostream>

// POSIX C
#include <syslog.h>

namespace UI {
namespace Util {

/** @brief C++ Abstraction of syslog(3) for mono threaded applications. */
class SysLogMono: public std::streambuf, public std::ostream
{
public:
	/**
	 * @param ident Identifier (prefix) for the log lines.
	 * @param option Options as described in syslog(3).
	 * @param facility Facility as described in syslog(3).
	 */
	SysLogMono(std::string const & ident, int option, int  facility);

	/** @brief Destructor. */
	~SysLogMono();

	/** @brief Log operator.
	 *
	 * @param level Log level as described in syslog(3).
	 */
	SysLogMono & operator() (int level);

private:
	int overflow(int c);
	std::string const ident_;
	std::string buf_;
	int level_;
};

/** @brief Singleton class holding one SysLogMono object. */
class SysLogMonoSingleton
{
public:
	/**
	 * @param ident Identifier (prefix) for the log lines.
	 * @param option Options as described in syslog(3).
	 * @param facility Facility as described in syslog(3).
	 */
	SysLogMonoSingleton(std::string const & ident, int option, int facility);

	/** */
	~SysLogMonoSingleton();

	/** @brief Log function; you may stream directly into the result.
	 *
	 * @param level Log level as described in syslog(3).
	 * @returns SysLogMono stream ready to stream into.
	 */
	static SysLogMono & log(int level);
private:
	static SysLogMono * singleton_;
};

}}
#endif
