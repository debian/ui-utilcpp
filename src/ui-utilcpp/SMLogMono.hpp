/**
 * @file SMLogMono.hpp
 * @author Schlund + Partner AG
 * @brief Syslog Macro Log: Simple logging based on compiler macros and syslog(3) for mono-threaded aplications
 * @see SMLog.hpp
 *
 * Synopsis:
 * @code
 * #include <ui-utilcpp/SMLogMono.hpp>
 * @endcode
 *
 * This can be used as drop-in replacement for SMLog.hpp, i.e. the source using these
 * logging macros does not need to be changed.
 *
 * Each prirority level (see syslog(3)) gets its own compiler macro, which can be used
 * rather straightforward in the source; the argument of the macro may include stream piping.
 * For example:
 *
 * SM_LOGNOTICE("I have noticed something: " << something)
 *
 * Logs can be turned off at compile time (giving full run time advantage, as the code is removed)
 * with a threshold. Additionally, you can give the facility, and additional text at compile time.
 * For example:
 *
 * c++ ... -DSM_LOGLEVEL=5 -DSM_LOGADD="getpid()" -DSM_LOGFACILITY="ost::SLOG_USER"
 *
 * eliminates all log messages with priority higher than 5, giving the process id as additional
 * information
 *
 * Logging itself is done via SysLogMonoSingleton; it should be configured
 * somewhere at the beginning of your program, for example like:
 *
 * @code
 * UI::Util::SysLogMonoSingleton logger("dvdb", LOG_PERROR, LOG_DAEMON);
 * @endcode
 *
 */

/**
 * @example SMLogMono.cpp
 * Example showing log messages in all levels.
 * Should be installed as ui-utilcpp-smlogmono along with the library.
 */

#ifndef UI_UTIL_SMLOGMONO_HPP
#define UI_UTIL_SMLOGMONO_HPP

// STDC++
#include <iostream>

// SYSTEM C
#include <syslog.h> // BSD

// Local
#include <ui-utilcpp/SysLogMono.hpp>

#ifndef SM_LOGLEVEL
/** @brief The Log Level; Log code under this level gets eliminated. */
#define SM_LOGLEVEL LOG_NOTICE
#endif

/** @brief Add this text to every log messages. */
#ifndef SM_LOGADD
#define SM_LOGADD __FILE__ << ":" << __LINE__
#endif

#ifndef SM_LOG
/** @brief Master Macro (must not be used directly). */
#ifdef SM_ENABLE_CLOG
#define SM_LOG(l, p, x) { UI::Util::SysLogMonoSingleton::log(l) << p << ": " << x << " [" SM_LOGADD << "]." << std::endl; std::clog << p << ": " << x << " [" SM_LOGADD << "]." << std::endl; }
#else
#define SM_LOG(l, p, x) UI::Util::SysLogMonoSingleton::log(l) << p << ": " << x << " [" SM_LOGADD << "]." << std::endl
#endif
#endif


/** @brief Macro for syslog(3) level EMERG. */
#if SM_LOGLEVEL >= LOG_EMERG
#define SM_LOGEMERG(x) SM_LOG(LOG_EMERG, "EMG", x)
#else
#define SM_LOGEMERG(x)
#endif

/** @brief Macro for syslog(3) level ALERT. */
#if SM_LOGLEVEL >= LOG_ALERT
#define SM_LOGALERT(x) SM_LOG(LOG_ALERT, "ALT", x)
#else
#define SM_LOGALERT(x)
#endif

/** @brief Macro for syslog(3) level CRIT. */
#if SM_LOGLEVEL >= LOG_CRIT
#define SM_LOGCRIT(x) SM_LOG(LOG_CRIT, "CRT", x)
#else
#define SM_LOGCRIT(x)
#endif

/** @brief Macro for syslog(3) level ERR. */
#if SM_LOGLEVEL >= LOG_ERR
#define SM_LOGERR(x) SM_LOG(LOG_ERR, "ERR", x)
#else
#define SM_LOGERR(x)
#endif

/** @brief Macro for syslog(3) level WARNING. */
#if SM_LOGLEVEL >= LOG_WARNING
#define SM_LOGWARNING(x) SM_LOG(LOG_WARNING, "WRN", x)
#else
#define SM_LOGWARNING(x)
#endif

/** @brief Macro for syslog(3) level NOTICE. */
#if SM_LOGLEVEL >= LOG_NOTICE
#define SM_LOGNOTICE(x) SM_LOG(LOG_NOTICE, "ntc", x)
#else
#define SM_LOGNOTICE(x)
#endif

/** @brief Macro for syslog(3) level INFO. */
#if SM_LOGLEVEL >= LOG_INFO
#define SM_LOGINFO(x) SM_LOG(LOG_INFO, "inf", x)
#else
#define SM_LOGINFO(x)
#endif

/** @brief Macro for syslog(3) level DEBUG. */
#if SM_LOGLEVEL >= LOG_DEBUG
#define SM_LOGDEBUG(x) SM_LOG(LOG_DEBUG, "dbg", x)
#else
#define SM_LOGDEBUG(x)
#endif

/** @name Additional debug levels 8-11.
 * @{ */
#if SM_LOGLEVEL >= 8
#define SM_LOGDEBUG1(x) SM_LOGDEBUG(x)
#else
#define SM_LOGDEBUG1(x)
#endif

#if SM_LOGLEVEL >= 9
#define SM_LOGDEBUG2(x) SM_LOGDEBUG(x)
#else
#define SM_LOGDEBUG2(x)
#endif

#if SM_LOGLEVEL >= 10
#define SM_LOGDEBUG3(x) SM_LOGDEBUG(x)
#else
#define SM_LOGDEBUG3(x)
#endif

#if SM_LOGLEVEL >= 11
#define SM_LOGDEBUG4(x) SM_LOGDEBUG(x)
#else
#define SM_LOGDEBUG4(x)
#endif
/** @} */

#endif
