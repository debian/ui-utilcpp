/**
 * @file
 * @brief auto_ptr compat
 *
 * - C++14 deprecates 'auto_ptr' in favor of 'unique_ptr' [and gcc6 does ugly warnings].
 * - std::unique_ptr is available since C++11, so we use it then.
 * - While compat is still needed, use 'UI::Util::auto_ptr' in-code.
 * - When removing this compat, just bulk-replace 'UI::Util::auto_ptr' with 'std::unique_ptr' in-code.
 */
#ifndef UI_UTIL_AUTO_PTR_COMPAT_HPP
#define UI_UTIL_AUTO_PTR_COMPAT_HPP

// STDC++
#include <memory>

namespace UI {
namespace Util {

#if __cplusplus >= 201103L
template <typename T>
using auto_ptr = std::unique_ptr<T>;
#else
using std::auto_ptr;
#endif

}}

#endif
